Ext.define('Ext.view.CalendarView',{
	extend : 'Ext.dataview.DataView',
	xtype : 'calendarview',
	config : {
		orientation : null,
		currentDate : null,
		dates : null,
		calendarWeek : null,
		records : null
	},
	initialize : function() {
		this.callParent();
		Ext.Viewport.on('orientationchange', 'handleOrientationChange', this, {buffer: 50 });

		var store = this.getStore();
		store.on('load', 'handleLoadedData', this);
		
		var me = this,
		myElement = me.element;

		myElement.on({
            scope : me,
            tap   : 'onElementTap',
            delegate : '.slot.content-available'
        });
	},
	onElementTap : function(eventObj){
		var eventTime, eventDateObj, day, hrs, concertDetailsMsgBox, tpl, msgBoxContent;

		eventTime = eventObj.target.getAttribute('data-time');
		eventDateObj  = new Date(eventTime);
		day = eventDateObj.getDay();
		hrs = eventDateObj.getHours();

		concertDetailsMsgBox = Ext.create("Ext.MessageBox", { message : ""});
		
		tpl = new Ext.XTemplate('<table id="msgBoxContent">',	'<tpl for=".">',
												'<tr><td>Event:</td><td>{content}</td></tr>',
												'<tr><td>Date:</td><td>{date}</td></tr>',
												'<tr><td>Address:</td><td>{address}</td></tr>','</tpl></table>');
		
		msgBoxContent = tpl.apply(this.getCalendarWeek()[day]['slots'][hrs]);
		
		concertDetailsMsgBox.show({
   			title: 'Concert Details',
  			height: '100%',
  			width: '100%',
  			message: msgBoxContent,
  			scrollable: 'vertical'
		})
	},//Render html calendar
	renderCalendar : function(orientation, calendarWeek){
		if(orientation == 'portrait'){
	    	var calendarWeek = calendarWeek.slice(1,6);
	    }
	    this.createCalendarHtml(calendarWeek);
	},
	handleOrientationChange: function(viewport, orientation, width, height){
	    this.setOrientation(orientation);
	   	this.renderCalendar(orientation, this.getCalendarWeek());	    
	},
	generateWeek : function(startDateObj){ 
		var week =[];
		
		//Grab year, month, day and create a new object
		startDateObj = new Date(startDateObj.getFullYear(), startDateObj.getMonth(), startDateObj.getDate());
		for (var i = 0; i < 7; i++){
			var numOfMilliseconds =  i != 6 ? i * 86400000 : i * 86400000 + 86399999; //last day of the week should end at 11:59:59
			var day = new Date(startDateObj.valueOf() + numOfMilliseconds );
			week.push(day);
		}

		return week;
	},
	calculateFirstDay : function(dateObj){
		var startDay = 0;
		var d = dateObj.getDay();

		var weekStart = new Date(dateObj.valueOf() - (d<=0 ? startDay:d-startDay)*86400000);

		return weekStart;
	},//Create blank data obj to hold concert data
	generateBlankCalendar : function(week){
		var calendarWeek = []; 
		for(var i = 0; i < week.length; i++){
			calendarWeek[i] = {};
			calendarWeek[i]['day'] = week[i];
			calendarWeek[i].slots = new Array(24);
			for(var j = 0; j < calendarWeek[i].slots.length; j++)
				calendarWeek[i].slots[j] = 0;
		}
		return calendarWeek;
	},

	populateCalendar : function(calendarWeek, dates, dateObj, firstDayWeek, lastDayWeek){
		for(var i = 0; i < dates.length; i++){
			var concertDateObj = new Date(dates[i].getData().date);
			
			var concertHour = concertDateObj.getHours(),
				concertDay = concertDateObj.getDay(),
				concertDate = concertDateObj.getDate(),
				concertMonth = concertDateObj.getMonth(),
				concertYear = concertDateObj.getFullYear();		

			for(var j = 0; j < calendarWeek.length; j++){

				if(	
					concertDateObj >= firstDayWeek && 
					concertDateObj <= lastDayWeek
					){

					calendarWeek[concertDay].slots[concertHour] =new Array();
					calendarWeek[concertDay].slots[concertHour].push(dates[i].getData());

				}
			}

		}
		
		return calendarWeek;
	},
	//Create calendar grid
	createDatesArray : function(dates, dateObj){
		var firstDay = this.calculateFirstDay(dateObj);
		var week = this.generateWeek(firstDay);
		var calendarWeek = this.generateBlankCalendar(week);
		var popCalendarWeek = this.populateCalendar(calendarWeek, dates, dateObj, firstDay, week[6]);

		this.setCalendarWeek(popCalendarWeek);

		return popCalendarWeek;
	},
	createCalendarHtml : function(dates){
		var tpl = new Ext.XTemplate('	<div id="calendarWrapper"><tpl for=".">',
											'<div class="day">',
												'{[(values.day.getMonth()+1) + "/" + values.day.getDate()]}',
												'<tpl for="slots">',
													'<tpl if="values != 0">',
														'<p class="slot content-available" data-time="{[values[0].date]}">',
															'{[xindex - 1]}',
													'<tpl else>',
														'<p class="slot content-unavailable" >',
														'{[xindex - 1]}',
													'</tpl>',
														'</p>',
												'</tpl>',
											'</div>',
										'</tpl></div>');

		var dataViewBody = this.element.down('.x-dataview-item');
		tpl.overwrite(dataViewBody, dates);

	},
	handleLoadedData : function(scope, records, successful, operation, eOpts){
		if(successful === true){
			this.setRecord(records);
			var calendar = this.createDatesArray(records, this.getCurrentDate());
			
			this.renderCalendar(this.getOrientation(), this.getCalendarWeek());
		}
	}
});
