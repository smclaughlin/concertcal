Sencha Touch Demo
====================

Instructions
---------------------

*   Copy all files to the document root of a webserver with php installed
*   Open /calendar.html page in a mobile webkit browser (preferably chrome) 
*   Use the next and back buttons to navigate the calendar
*   Click on a highlighted cell to view the details of the concert
*   Change the orientation of the screen and notice that in portrait mode MON-FRI is shown and in landscape the SUN-SAT is shown


