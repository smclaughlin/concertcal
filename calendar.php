<?php
	
	//fake mobile connection
	sleep(2);
	$dates = array(
		array('date' => 'July 1, 2014 17:15:00', 'content' => 'Busta rhymes concert', 'address' => 'The Shire'),
		array('date' => 'July 2, 2014 14:15:00', 'content' => 'ATCQ rhymes concert', 'address' => 'Oz'),
		array('date' => 'July 3, 2014 23:15:00', 'content' => 'Lauryn Hill concert', 'address' => 'Narnia'),
		array('date' => 'July 4, 2014 17:15:00', 'content' => 'Nickelback', 'address' => 'Krypton'),
		array('date' => 'July 5, 2014 14:15:00', 'content' => 'Linkin Park', 'address' => 'Saturn'),
		array('date' => 'July 6, 2014 23:15:00', 'content' => 'Lupe Fiasco', 'address' => 'Metropolis'),
		array('date' => 'July 7, 2014 17:15:00', 'content' => 'Maroon 5', 'address' => 'Camp Crystal Lake'),
		array('date' => 'July 8, 2014 14:15:00', 'content' => 'Jay Z', 'address' => 'Cybertron'),
		array('date' => 'July 9, 2014 23:15:00', 'content' => 'Bob Marley', 'address' => 'Jamaica')
	);
	
	echo json_encode(array(
		'dates' => $dates,
		'total' => count($dates),
		'success' => true
	));
?>
